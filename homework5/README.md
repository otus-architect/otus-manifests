# ДЗ Модуль 5. Основы работы с Kubernetes (часть 2)
#### Исходники
https://gitlab.com/otus-architect/otus-kubernetes-basics.git
 

#### Добавить ingress в minikube
minikube addons enable ingress

#### Создать namespace
kubectl create namespace otus-hw

#### Применть приложенный манифест

#### Проверочные запросы
Если запуск в миникубе, то выполнить minikube ip
Подставить полученный IP в запросы вместо 192.168.99.100

* `curl -H'Host: arch.homework' http://192.168.99.100/health`
* `curl -H'Host: arch.homework' http://192.168.99.100/otusapp/pupkin/info`
* `curl -H'Host: arch.homework' http://192.168.99.100/otusapp/pupkin/`
* `curl -H'Host: arch.homework' http://192.168.99.100/otusapp/pupkin`

Елси запуск в полноценном кластере с установленным DNS, то

* `curl http://arch.homework/health`
* `curl http://arch.homework/otusapp/pupkin/info`
* `curl http://arch.homework/otusapp/pupkin/`
* `curl http://arch.homework/otusapp/pupkin`
