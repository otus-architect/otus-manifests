# ДЗ Модуль 20. Идемпотетность и коммутативность API в HTTP и очередях


### Описание идемпотентного создания заказа
Рассматривалось три варианта внедрения идемпотентности при создании заказа:  
1. Idempotency key  
2. ETag в заголовке запросов (оптимистичная блокировка). Коллекции пользовательских заказов присваивается версия.
3. Fingerprint или Hash от запроса
Для метода создания заказа был выбран первый способ. Клиент генерит уникальный UUID и в каждом запросе вставляет его  
в заголовок. Первый способ был выбран потому что создание заказа не является критичной операцией. В самом худшем случае    
пользователь создаст копию заказа, но при этом в интерфейсе увидит что создалось два одинаковых заказа. Кроме того,  
основная доля покупок осуществляется со стационарных компьютеров, что также уменьшает относительное количество сетевых ошибок.  


### Подготовка к установке приложения
* Добавить arch.homework в /etc/hosts
* minikube delete && minikube start && minikube addons enable ingress
* helm repo add bitnami https://charts.bitnami.com/bitnami && helm repo update


### Установка приложения из helm
helm install pg bitnami/postgresql -f common-deployment-files/bitnami-postgres-values.yml && \
helm install rabbit bitnami/rabbitmq -f common-deployment-files/bitnami-rabbitmq-values.yml && \
kubectl apply -f common-deployment-files/job-postgres.yml && \
kubectl create namespace otus-hw && \
kubectl config set-context --current --namespace=otus-hw && \
kubectl apply -f common-deployment-files/cluster-role.yml && \
kubectl apply -f common-deployment-files/cluster-role-binding.yml && \
cd option1-helm && \
helm install otus-user-service ./otus-user-service/ && \
helm install otus-auth-service ./otus-auth-service/ && \
helm install otus-order-service ./otus-order-service/ && \
helm install otus-billing-service ./otus-billing-service/ && \
helm install otus-notification-service ./otus-notification-service/ && \
helm install otus-loyalty-service ./otus-loyalty-service/ && \
helm install otus-mobile-bff ./otus-mobile-bff/ && \
helm install otus-product-service ./otus-product-service/ && \
helm install otus-warehouse-service ./otus-warehouse-service/


### Запросы Postman
postman_collection.json