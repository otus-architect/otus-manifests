cd /home/serg/IdeaProjects/otus-manifests/homework36/ && minikube delete && minikube start && sleep 10 && \
helm install serg1 prometheus-community/kube-prometheus-stack -f common-deployment-files/prometheus-settings-values.yml && \
helm install serg2 ingress-nginx/ingress-nginx -f common-deployment-files/nginx-ingress-values.yml && \
helm install serg3 prometheus-community/prometheus-postgres-exporter -f common-deployment-files/postgres-exporter-values.yml && \
sleep 70 && \
helm install pg bitnami/postgresql -f common-deployment-files/bitnami-postgres-values.yml && \
helm install rabbit bitnami/rabbitmq -f common-deployment-files/bitnami-rabbitmq-values.yml && \
sleep 20 && \
kubectl apply -f common-deployment-files/job-postgres.yml && \
kubectl apply -f common-deployment-files/prometheus-grafana-dashboard.yml && \
kubectl create namespace otus-hw && \
kubectl config set-context --current --namespace=otus-hw && \
kubectl apply -f common-deployment-files/cluster-role.yml && \
kubectl apply -f common-deployment-files/cluster-role-binding.yml && \
cd option1-helm && \
helm install otus-user-service ./otus-user-service/ && \
helm install otus-auth-service ./otus-auth-service/ && \
helm install otus-order-service ./otus-order-service/ && \
helm install otus-billing-service ./otus-billing-service/ && \
helm install otus-notification-service ./otus-notification-service/ && \
helm install otus-loyalty-service ./otus-loyalty-service/ && \
helm install otus-mobile-bff ./otus-mobile-bff/ && \
helm install otus-product-service ./otus-product-service/ && \
helm install otus-warehouse-service ./otus-warehouse-service/ && \
helm install otus-delivery-service ./otus-delivery-service/