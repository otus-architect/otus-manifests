# ДЗ Модуль 28. Распределённые транзакции

### Описание выбранного решения
Для реализации распределённых транзакций был выбран паттерн Сага.  
Двухфазный коммит более сложен в реализации, а также в интернет-магазине отсутствует  
необходимость в строгой консистентности. Нас устроит консистентность "когда-нибудь",  
поэтому в качестве паттерна выбираем Сагу.

### Подготовка к установке
* Добавить arch.homework в /etc/hosts
* minikube delete && minikube start && minikube addons enable ingress
* helm repo add bitnami https://charts.bitnami.com/bitnami && helm repo update

### Установка
helm install pg bitnami/postgresql -f common-deployment-files/bitnami-postgres-values.yml && \
helm install rabbit bitnami/rabbitmq -f common-deployment-files/bitnami-rabbitmq-values.yml && \
kubectl apply -f common-deployment-files/job-postgres.yml && \
kubectl create namespace otus-hw && \
kubectl config set-context --current --namespace=otus-hw && \
kubectl apply -f common-deployment-files/cluster-role.yml && \
kubectl apply -f common-deployment-files/cluster-role-binding.yml && \
cd option1-helm && \
helm install otus-user-service ./otus-user-service/ && \
helm install otus-auth-service ./otus-auth-service/ && \
helm install otus-order-service ./otus-order-service/ && \
helm install otus-billing-service ./otus-billing-service/ && \
helm install otus-notification-service ./otus-notification-service/ && \
helm install otus-loyalty-service ./otus-loyalty-service/ && \
helm install otus-mobile-bff ./otus-mobile-bff/ && \
helm install otus-product-service ./otus-product-service/ && \
helm install otus-warehouse-service ./otus-warehouse-service/ && \
helm install otus-delivery-service ./otus-delivery-service/

### Проверка
saga.postman_collection.json
