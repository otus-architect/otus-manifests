# ДЗ Модуль 6. Основы работы с Kubernetes (часть 3). Инфраструктурные паттерны

### Исходники
https://gitlab.com/otus-architect/otus-user-service.git

### Запуск

--------------
* (optional) minikube addons enable ingress
* kubectl create namespace otus-hw
* kubectl config set-context --current --namespace=otus-hw
* helm repo add bitnami https://charts.bitnami.com/bitnami
* helm repo update
* cd otus-user-service/ 
* helm dependency update
* kubectl apply -f job-for-initialize.yml
* helm install otus-user-service ./otus-user-service/
* проверка работоспособности через postman_collection

### Зачистка

--------------
* helm uninstall otus-user-service
* kubectl delete job.batch/initdb
* kubectl delete --all pv --namespace=otus-hw
* kubectl delete --all pvc --namespace=otus-hw
* kubectl delete namespace otus-hw



