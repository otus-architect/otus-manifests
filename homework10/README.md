# ДЗ Модуль 14. Реализация паттерна CQRS с аудитом событий, но без Event Sourcing


### Описание как был реализован CQRS
Все операции были разбиты на две группы: read и write
Для операций группы read были созданы классы Query и соответсвующие обработчики. Каждый query возвращает результат. 
Для операций группы write были созданы классы Command и соответсвующие обработчики. Почти что каждая command ничего не возвращает,
т.к. является операцией модификации. Исключение составляет команда по созданию заказа, которая должна возвращать его номер.
1

### Предустановки
add arch.homework to your /etc/hosts 
minikube start
minikube addons enable ingress
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update

### Установка
helm install pg bitnami/postgresql -f db-setup/bitnami-postgres-values.yml && \
kubectl apply -f db-setup/job-postgres.yml && \
kubectl create namespace otus-hw && \
kubectl config set-context --current --namespace=otus-hw && \
cd helm-charts && \
helm install otus-user-service ./otus-user-service/ && \
helm install otus-auth-service ./otus-auth-service/ && \
helm install otus-order-service ./otus-order-service/

### Проверка
postman_collection.json

