# ДЗ Модуль 25. Паттерны декомпозиции микросервисов

## Тема: Интернет-Магазин

[Пользовательские сценарии (в нотации Gherkin)](USE_CASES.md)

[Системные действия](SYSTEM_ACTIONS.md)

[Доменная модель](DOMAIN_MODEL.md)

[Описание микросервисов](MICROSERVICES_DESCRIPTION.md)

[Канвасы микросервисов](MICROSERVICES_CANVAS.md)

[Задания на изменения текущей реализации](IMPLEMENTATION_TASKS.md)

