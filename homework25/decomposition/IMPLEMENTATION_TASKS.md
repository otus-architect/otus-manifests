# Задания на изменения текущей реализации по итогам проведённой декомпозиции

1. Объединить otus-auth-service и otus-user-service
2. Объединить otus-loaylty-service и otus-product-service
3. В otus-product-service реализовать метод полнотекстового поиска, а
также запросы в otus-prediction-service и otus-warehouse-service
4. В otus-warehouse-service реализовать резерв товара
5. В otus-product-service реализовать подсчёт общего рейтинга в агрегате product
2. Имплементировать otus-delivery-service
3. Имплементировать otus-prediction-service
