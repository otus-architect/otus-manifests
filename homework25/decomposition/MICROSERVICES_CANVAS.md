# Канвасы микросервисов

## otus-auth-service
![](.MICROSERVICES_CANVAS_images/35fc9710.png)

## otus-order-service
![](.MICROSERVICES_CANVAS_images/299ba014.png)

## otus-product-service
![](.MICROSERVICES_CANVAS_images/fd65b997.png)

## otus-warehouse-service
![](.MICROSERVICES_CANVAS_images/00431f77.png)

## otus-notification-service
![](.MICROSERVICES_CANVAS_images/72e7890c.png)

## otus-delivery-service
![](.MICROSERVICES_CANVAS_images/f0127620.png)

## otus-billing-service
![](.MICROSERVICES_CANVAS_images/d0ed23d3.png)

## otus-prediction-service
![](.MICROSERVICES_CANVAS_images/d3619358.png)

## otus-mobile-bff
![](.MICROSERVICES_CANVAS_images/75c1d170.png)

