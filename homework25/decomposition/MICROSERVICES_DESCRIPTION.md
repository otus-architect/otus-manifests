# Построение сервисной модели
## Разбиение на сервисы
otus-auth-service  
otus-order-service  
otus-product-service  
otus-warehouse-service  
otus-notification-service  
otus-delivery-service  
otus-billing-service  
otus-prediction-service  
otus-mobile-bff  


## Описание API
otus-auth-service:  
POST /api/v1/customer/registration -d {user}  
POST /api/v1/customer/login -d {credentials}  
GET /api/v1/auth -H {cookie}  
DELETE /api/v1/logout -H {cookie}  

otus-order-service:  
GET /api/v1/order/1  
GET /api/v1/order?created=10-02-2021&page=0&limit=2  
POST /api/v1/order -d {order}  
POST /api/v1/order/1/item -d {item}  
POST /api/v1/order/1/payment  
PUT /api/v1/order/1 -d {order}  
PUT /api/v1/order/1/item -d {item}  
DELETE /api/v1/order/1/item/2  
DELETE /api/v1/order/1  

otus-product-service:  
GET /api/v1/search/product?q=milk&region=moscow&page=0&limit=20  
GET /api/v1/search/product?category=milk&page=0&limit=20  
GET /api/v1/product/3  
POST /api/v1/product -d {product}  
PUT /api/v1/product/3 -d {product}  
DELETE /api/v1/product/3  
GET /api/v1/product/1/rating  
POST /api/v1/product/1/rating -d {rating}  
POST /api/v1/product/1/comment -d {comment}  
PUT /api/v1/product/1/comment -d {comment}  
PUT /api/v1/product/1/comment/1/card (привязать коммет)  
DELETE /api/v1/product/1/comment/1  

otus-warehouse-service:  
POST /api/v1/unit/1/reservation -d {reservation}  
GET /api/v1/unit/1  
POST /api/v1/unit -d {unit}  
PUT /api/v1/unit -d {unit}  
DELETE /api/v1/unit/{id}  

otus-notification-service:  
GET /api/v1/notification/1  
GET /api/v1/notification?userId=42  

otus-delivery-service:  
GET /api/v1/delivery/1  
POST /api/v1/delivery/{id}/return  

otus-billing-service:  

otus-prediction-service:  
GET /api/v1/prediction?productId=1&address=fdsf-owei-cvx  
GET /api/v1/prediction?productIds=1,5,8,90&address=fdsf-owei-cvx  

otus-mobile-bff:  
GET /api/v1/mobile/unit/1  

## Описание текстом: 
* Сервис биллинга. Все взаимодействия внутренние, статус оплаты получаем
из otus-order-service
* Сервис предсказаний. Дёргается при поиске, в результатах поиска сразу
будет предсказание
* Сервис доставки. Все формирования доставок внутренние. Наружу торчит
только получение статуса доставки
* Сервис нотификации. Создание нотификаций внутреннее. Наружу торчит
только получение созданных нотификаций
* Сервис склада. CRUD по добавлению админом единицы товара на склад. А также
резерв товара на складе после оплаты
* Сервис продуктов. CRUD по занесению админом новых товаров в систему. А также 
наружу торчит поиск товаров по разным критериям. Взаимодействует с
otus-prediction-service и otus-warehouse-service для более полной
выдачи покупателю
* Сервис заказов. CRUD по заказам и позициям в заказе. Поиск заказов. Оплата заказа
* Сервис лояльности. Объединям с otus-product-service. CRUD по созданию 
комментариев и рейтингов
* Сервис аутентификации. Регистрация пользователей, аутентификация пользователей,
получение информации о пользователях
* Сервис пользователей. Объединение otus-auth-service и otus-user-service 
* Сервис поиска. Не нужен. Мы добавим функционал 
полнотекстового поиска в otus-product-service