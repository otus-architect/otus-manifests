# ДЗ Модуль 25 (Совмещённый с модулем 12 BFF). Паттерны декомпозиции микросервисов. 


### Описание паттерна BFF
Запросы от мобилного клиента приходят на эндпоинт сервиса otus-mobile-bff.
Каждый поступивший запрос полагает HTTP вызовы других сервисов, а именно otus-loyalty-service, otus-product-service,
otus-warehouse-service.
Данные запросы делаются через Feign.
Данные запросы выполняются по айдишникам сервисов, которые регистрируются в Kubernetes Discovery.


### Подготовка к установке приложения
* Добавить arch.homework в /etc/hosts
* minikube delete && minikube start && minikube addons enable ingress
* helm repo add bitnami https://charts.bitnami.com/bitnami && helm repo update


### Установка приложения из helm
helm install pg bitnami/postgresql -f common-deployment-files/bitnami-postgres-values.yml && \
helm install rabbit bitnami/rabbitmq -f common-deployment-files/bitnami-rabbitmq-values.yml && \
kubectl apply -f common-deployment-files/job-postgres.yml && \
kubectl create namespace otus-hw && \
kubectl config set-context --current --namespace=otus-hw && \
kubectl apply -f common-deployment-files/cluster-role.yml && \
kubectl apply -f common-deployment-files/cluster-role-binding.yml && \
cd option1-helm && \
helm install otus-user-service ./otus-user-service/ && \
helm install otus-auth-service ./otus-auth-service/ && \
helm install otus-order-service ./otus-order-service/ && \
helm install otus-billing-service ./otus-billing-service/ && \
helm install otus-notification-service ./otus-notification-service/ && \
helm install otus-loyalty-service ./otus-loyalty-service/ && \
helm install otus-mobile-bff ./otus-mobile-bff/ && \
helm install otus-product-service ./otus-product-service/ && \
helm install otus-warehouse-service ./otus-warehouse-service/


### Запросы Postman
postman_collection.json