# ДЗ Модуль 16. Event Collaboration cтиль взаимодействия с использованием брокера сообщений RabbitMQ
Домашняя работа по курсу Архитектор ПО. 
Занятие 16. Паттерны поддержания консистентности данных (Stream processing) 

### Теоретическая часть
Данная реализация является имплементацией паттерна Event Collaboration.
1. Пользователь регистрируется в системе. После вызова контроллера, из otus-auth-service в брокер отправляется 
событие в топик REGISTRATION_REQUESTED в RabbitMQ. Данный топик слушается сервисом otus-user-service где происходит сохранение 
пользователя в БД. После чего otus-user-service отправляет в топик USER_CREATED, который слушается сервисом otus-billing-service, 
где уже происходит создание аккаунта с нулевым балансом. 
2. Пользователь отправляет созданный заказ на оплату. После вызова контроллера, из otus-order-service в брокер отправляется 
событие в топик PAYMENT_REQUESTED, который слушается сервисом otus-billing-service. Далее он после успешного списания денег 
формирует email сообщение, однако на данный момент у сервиса биллинга есть только userId и нет ФИО пользователя, поэтому 
созданное email сообщение надо обагатить в otus-user-service. Посылаем туда event, при получении которого email сообщение 
обогощается ФИО и адресом почты, далее создаётся событие для otus-notification-service, где уже происходит отправка 
обогащённого сообщения на почту клиенту. 

Sequence диаграмма межсервисного взаимодействия
![](.README_images/59e0706a.png)

Sequence диаграмма оплаты заказа
![](.README_images/1b77a12c.png)

### Подготовка
* Добавить arch.homework в /etc/hosts
* minikube delete && minikube start && minikube addons enable ingress
* helm repo add bitnami https://charts.bitnami.com/bitnami && helm repo update

### Установка из хелм helm
helm install pg bitnami/postgresql -f db-setup/bitnami-postgres-values.yml && \
helm install rabbit bitnami/rabbitmq -f mq-rabbit-setup/rabbitmq-values.yml && \
kubectl apply -f db-setup/job-postgres.yml && \
kubectl create namespace otus-hw && \
kubectl config set-context --current --namespace=otus-hw && \
cd option1-helm && \
helm install otus-user-service ./otus-user-service/ && \
helm install otus-auth-service ./otus-auth-service/ && \
helm install otus-order-service ./otus-order-service/ && \
helm install otus-billing-service ./otus-billing-service/ && \
helm install otus-notification-service ./otus-notification-service/

### Проверка
postman_collection.json


