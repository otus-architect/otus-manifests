# ДЗ Модуль 9. Prometheus. Grafana

### Исходники
https://gitlab.com/otus-architect/otus-prometheus.git

### Подготовка хелма
* helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
* helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
* helm repo add bitnami https://charts.bitnami.com/bitnami
* helm repo update


### Установка 
kubectl create namespace otus-hw && \
kubectl config set-context --current --namespace=otus-hw && \
kubectl apply -f grafana-config-map.yaml && \
kubectl apply -f job-for-initialize.yml && \
cd otus-user-service/ && \
helm dependency update && \
cd .. && \
helm install otus-user-service ./otus-user-service/


### Проверка работоспособности
* curl -H'Host: arch.homework' http://192.168.49.2/api/v1/users/pre__initialized__user

### Запуск графаны
* kubectl port-forward service/otus-user-service-grafana 9000:80 -n otus-hw
* Перейти на localhost:9000 
* Нагрузить приложение для демонстрации:
`while :; do ab -n 50 -c 5 -H'Host: arch.homework' http://192.168.49.2/api/v1/users/pre__initialized__user ; sleep 1 ; done`


### Зачистка
* helm uninstall otus-user-service
* kubectl delete job.batch/initdb
* kubectl delete --all pv --namespace=otus-hw
* kubectl delete --all pvc --namespace=otus-hw
* kubectl delete namespace otus-hw



