# ДЗ Модуль 12. Backend for frontends. Apigateway
На самом деле тут нету BFF, эта домашка чисто про ораганизацию Security сервиса

### Исходники проекта
https://gitlab.com/otus-architect/otus-security

### Схема
![](.README_images/7e4ccf3f.png)

### Предустановка
* В /etc/hosts добавить minikube ip, смапленный на arch.homework
* minikube addons enable ingress
* helm repo add bitnami https://charts.bitnami.com/bitnami
* helm repo update

### Установка

helm install pg bitnami/postgresql -f bitnami-postgres-values.yml && \
kubectl apply -f job-init.yml && \
kubectl create namespace otus-hw && \
kubectl config set-context --current --namespace=otus-hw && \
helm install otus-user-service ./otus-user-service/ && \
helm install otus-auth-service ./otus-auth-service/

### Проверка

postman_collection.json
