# ДЗ Модуль 10. Service mesh на примере Istio

### Исходники

https://gitlab.com/otus-architect/otus-mesh.git

### Описание

Для установки ingress пользовался мануалами:
https://istio.io/latest/docs/tasks/traffic-management/ingress/ingress-control/
https://istio.io/latest/docs/reference/config/networking/virtual-service/#HTTPRoute

Сама настройка ingress gateway происходит в файлах:
 - gateway.yaml
 - virtual-service.yaml
 - destination-rule.yaml

Балансировка может настраиваться и куберовским сервисом (просто надо указать app selector выбирающий оба пода).
Но в данном случае балансировка настраивается на гейтвее, для чего был создан сам гейтвей (gateway.yaml),
был создан virtual service, в котором настраиваются правила раутинга и веса, а также был создан
destination rule для того чтобы определить subset'ы т.е. версии приложения